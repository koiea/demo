package com.example.demo.mapper;


import com.example.demo.bean.AccInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EsRepository  extends ElasticsearchRepository<AccInfo,Integer>{                   //这个一定得确定泛型
}
