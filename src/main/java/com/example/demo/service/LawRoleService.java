package com.example.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.bean.LawRole;
import com.example.demo.bean.LawRolePermission;
import com.example.demo.bean.LawRoleWithUser;
import com.example.demo.bean.LawUserRole;
import com.example.demo.mapper.LawRoleMapper;
import com.example.demo.mapper.LawRolePermissionMapper;
import com.example.demo.mapper.LawUserRoleMapper;
import com.example.demo.util.CommonResult;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
@Service
public class LawRoleService extends ServiceImpl<LawRoleMapper, LawRole>{
    @Autowired
    private LawRoleMapper lawRoleMapper;
    @Autowired
    private LawRolePermissionMapper lawRolePermissionMapper;
    @Autowired
    private LawUserRoleMapper lawUserRoleMapper;

    public IPage getRoleTable(Long page,Long limit){
        IPage<LawRole> iPage= lawRoleMapper.selectPage(new Page<LawRole>(page,limit) ,new QueryWrapper<LawRole>().orderByAsc("idlaw_role"));
        return iPage;
    }

    @CacheEvict(value = "User",allEntries=true)
    public int editRolePermission(int id,String checkData){
        lawRolePermissionMapper.deleteByLawRoleId(id);
        JSONArray jsonArray=JSONArray.parseArray(checkData);
        for(int i=0;i<jsonArray.size();i++) {
            lawRolePermissionMapper.insertLawRolePermissionMapper(id, jsonArray.getInteger(i));
        }
        return 0;
    }

    public CommonResult<JSONArray> getRolePermissionByRoleId(Long id){
        List<LawRolePermission> list=lawRolePermissionMapper.selectList(new QueryWrapper<LawRolePermission>().select("idlaw_permission").eq("idlaw_role",id));
        JSONArray jsonArray=new JSONArray();
        for(LawRolePermission l : list){
            jsonArray.add(l.getIdlawPermission());
        }
        return CommonResult.success(jsonArray);
    }


    public CommonResult addRole(String role){
        lawRoleMapper.insert(new LawRole().setRole(role));
        return CommonResult.success(null);
    }

    public CommonResult deleteRole(Long id){
        List<LawUserRole> list=lawUserRoleMapper.selectList(new QueryWrapper<LawUserRole>().eq("idlaw_role",id));
        if(list.size()>0){
            return  CommonResult.failed();
        }
        lawRolePermissionMapper.deleteByLawRoleId(id);
        return CommonResult.success(lawRoleMapper.delete(new QueryWrapper<LawRole>().eq("idlaw_role",id)));
    }

    public Map getRoleTableWithUserId(Long idUser, Long page, Long limit){
        List<LawUserRole> lawUserRoleList=lawUserRoleMapper.selectList(new QueryWrapper<LawUserRole>().eq("idlaw_user",idUser));
        IPage<LawRole> iPage= lawRoleMapper.selectPage(new Page<LawRole>(page,limit) ,new QueryWrapper<LawRole>().orderByAsc("idlaw_role"));
        List<LawRoleWithUser> list=new ArrayList<LawRoleWithUser>();
        for(LawRole lawRole: iPage.getRecords()){
            LawRoleWithUser lawRoleWithUser=new LawRoleWithUser();
            lawRoleWithUser.setRole(lawRole.getRole());
            lawRoleWithUser.setIdlawRole(lawRole.getIdlawRole());
            list.add(lawRoleWithUser);
        }
        for(LawRoleWithUser lawRole: list) {
            for (LawUserRole lawUserRole : lawUserRoleList) {
                if (lawRole.getIdlawRole() == lawUserRole.getIdlawRole()) {
                    lawRole.setToUser(1);
                }
            }
        }
        Map map=new HashMap();
        map.put("size",iPage.getTotal());
        map.put("data",list);
        return map;
    }

}
