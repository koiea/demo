package com.example.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.LawServ;
import com.example.demo.bean.LawServType;
import com.example.demo.mapper.LawServMapper;
import com.example.demo.util.GetLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;



@Service
public class LawSerService {
    @Autowired
    LawServMapper lawServMapper;


    @Autowired
    StorageService storageService;






    public int addLawServ(String text,String price,String content,int id,MultipartFile file,int enable){
        lawServMapper.insertLawServ(text,storageService.store(file),price,content,id,enable);
        return 0;
    }


    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    public List<LawServ> getAllLawServ(){
        return lawServMapper.selectAllbyEnable("%");
    }

    public List<LawServ> getEnableLawServ(){
        return lawServMapper.selectAllbyEnable("1");
    }

    public JSONArray getAllLAwServJSA(){
        JSONArray jsonArray=new JSONArray();
        List<LawServ> lawServList=getAllLawServ();

        for(LawServ lawServ:lawServList){
            JSONObject jso= new JSONObject();
            jso.put("id",lawServ.getId());
            jso.put("text",lawServ.getText());
            jso.put("price",lawServ.getPrice());
            jso.put("content",lawServ.getContent());
            jso.put("type",lawServ.getType());
            jso.put("idlaw_servtype",lawServ.getIdlaw_servtype());
            jso.put("enable",lawServ.getEnable());
            jso.put("ctime",lawServ.getCtime());
            jso.put("etime",lawServ.getEtime());

            String[] url=lawServ.getUrl().split("\\\\",0);
            System.out.println(url[url.length-1]);
            jso.put("url","./images/"+url[url.length-1]);
            jsonArray.add(jso);
        }
        return jsonArray;
    }

//    public JSONArray getAllLAwServJSAByGroup(){
//        JSONArray jsonArray=new JSONArray();
//        List<LawServ> lawServList=getAllLawServ();
//
//        int i=0;
//        JSONArray jsonArrayTemp=new JSONArray();
//        int l=lawServList.size();
//        for(LawServ lawServ:lawServList){
//            if(i<3){
//                JSONObject jso= new JSONObject();
//                jso.put("id",lawServ.getId());
//                jso.put("text",lawServ.getText());
//                jso.put("price",lawServ.getPrice());
//                jso.put("content",lawServ.getContent());
//                jso.put("type",lawServ.getType());
//                String[] url=lawServ.getUrl().split("\\\\",0);
//                System.out.println(url[url.length-1]);
//                jso.put("url","./images/"+url[url.length-1]);
//                jsonArrayTemp.add(jso);
//                i++;
//            }else{
//                i=0;
//                jsonArray.add(jsonArrayTemp);
//                jsonArrayTemp=new JSONArray();
//                JSONObject jso= new JSONObject();
//                jso.put("id",lawServ.getId());
//                jso.put("text",lawServ.getText());
//                jso.put("price",lawServ.getPrice());
//                jso.put("content",lawServ.getContent());
//                jso.put("type",lawServ.getType());
//                String[] url=lawServ.getUrl().split("\\\\",0);
//                System.out.println(url[url.length-1]);
//                jso.put("url","./images/"+url[url.length-1]);
//                jsonArrayTemp.add(jso);
//                i++;
//            }
//        }
//        while(i<3){
//            JSONObject jso= new JSONObject();
//            jso.put("text","");
//            jso.put("url","np");
//            jsonArrayTemp.add(jso);
//            i++;
//        }
//        jsonArray.add(jsonArrayTemp);
//        return jsonArray;
//    }




    public int deleteLawServ(int id){
        storageService.deleteOne(lawServMapper.getById(id).getUrl());
        lawServMapper.delete(id);
        return 0;
    }

    public JSONArray getLawServType(){
        List<LawServType> lawServTypeList=lawServMapper.selectType();
        JSONArray jsonArray=new JSONArray();
        for(LawServType lawServType:lawServTypeList){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",lawServType.getId());
            jsonObject.put("type",lawServType.getType());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public String getSingle(int id){
        LawServ lawServ=lawServMapper.selectSingle(id);
        lawServ.setUrl(GetLink.getVirtualLink(lawServ.getUrl()));
        return JSONObject.toJSONString(lawServ);
    }

    public int editLawServ(int id,String text,String price,String content,int typeid,int enable){

        return lawServMapper.updateLawServ(id,text,price,content,typeid,enable);
    }

    public int editLawServPic(MultipartFile file,int id){
        storageService.deleteOne(lawServMapper.getById(id).getUrl());
        String url=storageService.store(file);
        return lawServMapper.updateLawServPic(url,id);
    }
}
