package com.example.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.service.NewsService;
import com.example.demo.util.LayTableWrapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Api(tags = "新闻控制类")
@RestController
public class NewsController {
    @Autowired
    NewsService newsService;

    @RequestMapping("saveNews")
    public void saveNews(@RequestParam("title") String title,@RequestParam("content") String content,@RequestParam("date") String date,@RequestParam("sample") String sample,@RequestParam("type") String type,@RequestParam("mainpage") int mainpage){
        newsService.saveNews(title,date,sample,content,mainpage,type);
    }

    @RequestMapping("getNewsSampleTable")
    public String getNewsSampleTable(){
        JSONArray jsonArray=newsService.getNewsSample();
        JSONObject jsonObject=new LayTableWrapper().layTableJSO(jsonArray.size());
        jsonObject.put("data",jsonArray);
        System.out.println(jsonObject.toJSONString());
        return jsonObject.toJSONString();
    }
    @RequestMapping("getNewsSample")
    public String getNewsSample(){
        return newsService.getNewsSample().toJSONString();
    }

    @RequestMapping("getMainpageNewsSample")
    public String getMainpageNewsSample(){
        return newsService.getMainpageNewsSample().toJSONString();
    }

    @RequestMapping("getNewsContent")
    public String getNewsContent(@RequestParam("id") int id){
        return newsService.getNewsContent(id).toJSONString();
    }

    @RequestMapping("delNews")
    public int delNews(@RequestParam("id") int id){
        return newsService.delNews(id);
    }

    @RequestMapping("editNews")
    public int editNews(@RequestParam("id") int id,@RequestParam("title") String title,@RequestParam("date") String date,@RequestParam("sample") String sample,@RequestParam("content") String content,@RequestParam("mainpage") int mainpage,@RequestParam("type") String type){
        return newsService.editNews(id,title,date,sample,content,mainpage,type);
    }
}
