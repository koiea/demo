package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.LawServ;
import com.example.demo.service.LawSerService;
import com.example.demo.service.StorageFileNotFoundException;
import com.example.demo.service.StorageService;
import com.example.demo.util.GetLink;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


@Api(tags = "法律服务控制类")
@RestController
public class LawServicesController {

    @Autowired
    LawSerService lawSerService;




    @PostMapping("/addLawService")
    public int addLawService(@RequestParam("enable") int enable,@RequestParam("file") MultipartFile file,@RequestParam("text") String text,@RequestParam("price") String price,@RequestParam("content") String cotent,@RequestParam("typeid") int id) {
        lawSerService.addLawServ(text,price,cotent,id,file,enable);
        return 0;
    }


//    @PostMapping("/getLawServByGroup")
//    public String getLawServItemForPage(){
//        return lawSerService.getAllLAwServJSAByGroup().toJSONString();
//    }




    @PostMapping("/getLawServ")
    public String getLawServTable(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        JSONArray jsonArray=lawSerService.getAllLAwServJSA();
        jsonObject.put("data",jsonArray);
        jsonObject.put("count",jsonArray.size());
        return jsonObject.toJSONString();
    }

    @PostMapping("getEnableLawServ")
    public String getEnableLawServ(){
        List<LawServ> lawServList=lawSerService.getEnableLawServ();
        for(LawServ lawServ:lawServList){
            lawServ.setUrl(GetLink.getVirtualLink(lawServ.getUrl()));
        }
        return JSON.toJSONString(lawServList);
    }

    @PostMapping("/delLawServ")
    public int delLawServ(@RequestParam("id") int id){
       return lawSerService.deleteLawServ(id);
    }


    @PostMapping("getLawServType")
    public String getLawType(){
        return lawSerService.getLawServType().toJSONString();
    }

    @PostMapping("getLawServSingle")
    public String getSingle(int id){
        return lawSerService.getSingle(id);
    }

    @PostMapping("editLawServ")
    public int editLawServ(@RequestParam("id") int id,@RequestParam("enable") int enable,@RequestParam("text") String text,@RequestParam("price") String price,@RequestParam("content") String cotent,@RequestParam("typeid") int typeid){
        return lawSerService.editLawServ(id,text,price,cotent,typeid,enable);
    }

    @PostMapping("editLawServPic")
    public int editLawServPic(@RequestParam("file") MultipartFile file,@RequestParam("id") int id){
        return lawSerService.editLawServPic(file,id);
    }
}
