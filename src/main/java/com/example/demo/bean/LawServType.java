package com.example.demo.bean;


import io.swagger.annotations.ApiModel;

@ApiModel(value="法律服务类别对象", description="")
public class LawServType {
    int id;
    String type;

    public LawServType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LawServType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
