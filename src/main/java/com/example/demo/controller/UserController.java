package com.example.demo.controller;

import com.example.demo.bean.User;
import com.example.demo.service.UserService;
import com.example.demo.util.MD5Tool;
import io.swagger.annotations.Api;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.rmi.server.ExportException;

@Api(tags = "用户控制类")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("getUserTable")
    public String getUserTable(){
        return  userService.getAllUserTable().toJSONString();
    }
    @RequestMapping("searchUser")
    public String searchUser(@RequestParam("username") String name,@RequestParam("addr") String addr){
        return userService.getUserByNameAddr(name,addr).toJSONString();
    }


    @PostMapping("addAccount")
    public int addAccount(@RequestBody User user){


        rabbitTemplate.convertAndSend("emailDirectExchange", "tempEmail", user,message -> {
                    MessageProperties messageProperties = message.getMessageProperties();
                    // 设置这条消息的过期时间
                    messageProperties.setExpiration("5000");
                    return message;
                }
);


//        return userService.addAccount(name,password,sex,cp,addr,state,level);
        return 0;
    }
    @RequestMapping("editUser")
    public int editAccount(@RequestParam("field") String field, @RequestParam("id") int id, @RequestParam("value") String value){
        if(field.equals("sex")||field.equals("state")){
            return userService.editAccount(id,field,Integer.parseInt(value));
        }else{
            return userService.editAccount(id,field,value);
        }
    }
    @RequestMapping("delUser")
    public int delAccount(@RequestParam("id") int id){
        return userService.delAccount(id);
    }
    @RequestMapping("editPWAccount")
    public int editPWAccount(@RequestParam("id") int id,@RequestParam("opw") String opw,@RequestParam("npw") String npw){
        return userService.editPWAccount(id,opw,npw);
    }
}
