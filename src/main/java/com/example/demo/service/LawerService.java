package com.example.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.Lawer;
import com.example.demo.mapper.LawerMapper;
import com.example.demo.util.GetData;
import com.example.demo.util.GetLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class LawerService {
    @Autowired
    LawerMapper lawerMapper;

    @Autowired
    StorageService storageService;



    public int changeLawerIcon(MultipartFile file,int id){
        String url=storageService.store(file);
        storageService.deleteOne(lawerMapper.selectSingleLawer(id).getImgurl());
        return lawerMapper.updateSingleLawer(id,"imgurl",url);
    }



    public int addLawer(String name,String position,String introduction,MultipartFile file){
        String imgurl=storageService.store(file);
        lawerMapper.insertLawer(name,position,introduction,imgurl);
        return 0;
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    public JSONArray getAllLawer(){
        JSONArray jsonArray=new JSONArray();
        List<Lawer> lawers=lawerMapper.selectAllLawer();
        for(Lawer lawer:lawers){
            System.out.println("123"+lawer.getImgurl());
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("name",lawer.getName());
            jsonObject.put("id",lawer.getId());
            jsonObject.put("position",lawer.getPosition());
            jsonObject.put("introduction",lawer.getIntroduction());
            jsonObject.put("imgurl", GetLink.getVirtualLink(lawer.getImgurl()));
            jsonObject.put("ctime",lawer.getCtime());
            jsonObject.put("etime",lawer.getEtime());
            jsonObject.put("enable",lawer.getEnable());
            jsonObject.put("mainpage",lawer.getMainpage());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public JSONArray getMainpageLawer(){
        JSONArray jsonArray=new JSONArray();
        List<Lawer> lawers=lawerMapper.selectMainpageLawer();
        for(Lawer lawer:lawers){
            System.out.println("123"+lawer.getImgurl());
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("name",lawer.getName());
            jsonObject.put("id",lawer.getId());
            jsonObject.put("position",lawer.getPosition());
            jsonObject.put("introduction",lawer.getIntroduction());
            jsonObject.put("imgurl", GetLink.getVirtualLink(lawer.getImgurl()));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public JSONArray getEnableLawer(){
        JSONArray jsonArray=new JSONArray();
        List<Lawer> lawers=lawerMapper.selectEnableLawer();
        for(Lawer lawer:lawers){
            System.out.println("123"+lawer.getImgurl());
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("name",lawer.getName());
            jsonObject.put("id",lawer.getId());
            jsonObject.put("position",lawer.getPosition());
            jsonObject.put("introduction",lawer.getIntroduction());
            jsonObject.put("imgurl", GetLink.getVirtualLink(lawer.getImgurl()));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public int delLawer(int id){
        storageService.deleteOne(lawerMapper.selectSingleLawer(id).getImgurl());
        lawerMapper.delLawer(id);
        return 0;
    }


    public int updateSingleLawer(int id,String field,String value){
        return lawerMapper.updateSingleLawer(id,field,value);
    }
}
