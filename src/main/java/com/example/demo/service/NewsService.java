package com.example.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.util.GetLink;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class NewsService {

    @Autowired
    NewsMapper newsMapper;

    public int saveNews(String title,String date,String sample,String content,int mainpage,String type){
        newsMapper.insertNews(title,date,sample,content,type,mainpage);
        return 0;
    }

    @PreAuthorize("hasAnyAuthority('law_news:select')")
    public JSONArray getNewsSample(){
        List<News> newsList=newsMapper.selectNewsSample();
        JSONArray jsonArray=new JSONArray();
        for(News news:newsList){
            System.out.println(news);
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",news.getIdlawNews());
            jsonObject.put("title",news.getTitle());
            jsonObject.put("date",news.getDate());
            jsonObject.put("sample",news.getSample());
            jsonObject.put("ctime",news.getCtime());
            jsonObject.put("etime",news.getEtime());
            jsonObject.put("type",news.getType());
            jsonObject.put("mainpage",news.getMainpage());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public JSONArray getMainpageNewsSample(){
        List<News> newsList=newsMapper.selectMainpageNewsSample();
        JSONArray jsonArray=new JSONArray();
        for(News news:newsList){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",news.getIdlawNews());
            jsonObject.put("title",news.getTitle());
            jsonObject.put("date",news.getDate());
            jsonObject.put("sample",news.getSample());
            jsonObject.put("mainpage",news.getMainpage());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public JSONObject getNewsContent(int id){
        News news=newsMapper.selectNewsContent(id);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("id",news.getIdlawNews());
        jsonObject.put("title",news.getTitle());
        jsonObject.put("date",news.getDate());
        jsonObject.put("content",news.getContent());
        jsonObject.put("ctime",news.getCtime());
        jsonObject.put("etime",news.getCtime());
        jsonObject.put("type",news.getType());
        jsonObject.put("mainpage",news.getMainpage());
        return jsonObject;
    }

    public int delNews(int id){
        newsMapper.delNews(id);
        return 0;
    }

    public int editNews(int id,String title,String date,String sample,String content,int mainpage,String type){
        return newsMapper.updateNews(id,title,date,sample,content,mainpage,type);
    }




}
