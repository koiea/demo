package com.example.demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GetData {
    static public String getData(){
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");

        return ft.format(dNow);
    }
}
