package com.example.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.service.LawerService;
import com.example.demo.util.LayTableWrapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "律师控制类")
@RestController
public class LawerController {
    @Autowired
    LawerService lawerService;

    @RequestMapping("addLawer")
    public int addLawer(@RequestParam("file")MultipartFile file,@RequestParam("name") String name,@RequestParam("position") String position,@RequestParam("introduction") String introduction){
        return lawerService.addLawer(name,position,introduction,file);
    }

    @RequestMapping("getLawer")
    public String getLawer(){
        JSONArray jsonArray=lawerService.getAllLawer();
        JSONObject jsonObject=new LayTableWrapper().layTableJSO(jsonArray.size());
        jsonObject.put("data",jsonArray);
        return jsonObject.toJSONString();
    }

    @RequestMapping("delLawer")
    public int deleteLawer(@RequestParam("id") int id){
        lawerService.delLawer(id);
        return 0;
    }

    @RequestMapping("getMainPageLawer")
    public JSONArray getMainPageLawer(){
        return lawerService.getMainpageLawer();
    }

    @RequestMapping("getPageLawer")
    public JSONArray getPageLawer(){
        return lawerService.getEnableLawer();
    }


    @RequestMapping("editSingleLawer")
    public int eidtSingleLawer(@RequestParam("id") int id,@RequestParam("field") String field,@RequestParam("value") String value){
        return lawerService.updateSingleLawer(id,field,value);
    }

    @RequestMapping("changeLawerIcon")
    public int changeLawerIcon(@RequestParam("id") int id,@RequestParam("file") MultipartFile multipartFile){
        return lawerService.changeLawerIcon(multipartFile,id);
    }
}
