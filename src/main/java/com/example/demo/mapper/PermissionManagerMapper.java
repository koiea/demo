package com.example.demo.mapper;

import com.example.demo.bean.LawPermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface PermissionManagerMapper {


    public List<LawPermission> selectPermissionByIdlaw_user(int idlaw_user);

}
