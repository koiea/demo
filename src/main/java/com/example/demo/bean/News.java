package com.example.demo.bean;


import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value="新闻对象", description="")
@Data
public class News {
    Integer idlawNews;
    String title;
    String date;
    String sample;
    String content;
    String ctime;
    String etime;
    int mainpage;
    String type;

}
