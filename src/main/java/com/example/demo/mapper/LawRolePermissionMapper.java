package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.bean.LawRolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */

@Mapper
@Repository
public interface LawRolePermissionMapper extends BaseMapper<LawRolePermission> {
    public int insertLawRolePermissionMapper(long idLawRole,long idLawPermission);
    public int deleteByLawRoleId(long idLawRole);
}
