package com.example.demo.bean;


import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LawPermission对象", description="")
public class LawPermission implements Serializable, GrantedAuthority {

    private static final long serialVersionUID=1L;

    private Integer idlawPermission;

    private String permission;

    @Override
    public  String getAuthority(){
        return permission;
    }


}
