package com.example.demo.mapper;


import com.example.demo.bean.CustomMessage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CustomMessageMapper {
    public int insert(String name,String phone,String email,String message);
    public List<CustomMessage> selectAll();
    public int delete(int id);

}

