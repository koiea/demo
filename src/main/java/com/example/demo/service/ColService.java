package com.example.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.Col;
import com.example.demo.mapper.ColMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColService {

    @Autowired
    ColMapper colMapper;
    
    public JSONArray getColJSA(){
        JSONArray jsonArray=new JSONArray();
        List<Col> colList=colMapper.getColList();

        for(Col col:colList){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",col.getId());
            jsonObject.put("username",col.getCol());
            jsonArray.add(jsonObject);
            System.out.println(col.toString());
        }
        return jsonArray;
    }

    public int delCol(int id){
        return colMapper.delCol(id);
    }

    public int insertCol(){
        return colMapper.insertCol();
    }

    public int updateCol(int id,String col){
        return colMapper.updateCol(id,col);
    }
}
