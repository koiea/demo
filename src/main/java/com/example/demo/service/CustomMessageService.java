package com.example.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.CustomMessage;
import com.example.demo.mapper.CustomMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomMessageService {
    @Autowired
    private CustomMessageMapper customMessageMapper;

    public int insertToDB(String name,String phone,String email,String message){
        customMessageMapper.insert(name,phone,email,message);
        return 0;
    }

    public JSONArray getAllCustomMessage(){
        List<CustomMessage> customMessagesList=customMessageMapper.selectAll();
        JSONArray jsonArray=new JSONArray();
        for(CustomMessage customMessage:customMessagesList){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",customMessage.getId());
            jsonObject.put("name",customMessage.getName());
            jsonObject.put("phone",customMessage.getPhone());
            jsonObject.put("email",customMessage.getEmail());
            jsonObject.put("message",customMessage.getMessage());
            jsonArray.add(jsonObject);
        }
        return  jsonArray;
    }

    public int deleteCustomMessage(int id){
       return customMessageMapper.delete(id);
    }
}
