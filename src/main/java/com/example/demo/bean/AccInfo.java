package com.example.demo.bean;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Document(indexName = "acc_info", shards = 2, replicas = 1)
public class AccInfo {
    private Long id;
    private String ip;
    private String data;
    private String username;
}
