package com.example.demo.mapper;

import com.example.demo.bean.LawServ;
import com.example.demo.bean.LawServType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface LawServMapper {
    int insertLawServ(String text,String url,String price,String content,int id,int enable);
    List<LawServ> selectAllbyEnable(String enable);
    int delete(int id);
    LawServ getById(int id);
    List<LawServType> selectType();
    LawServ selectSingle(int id);
    int updateLawServ(int id,String text,String price,String content,int typeid,int enable);
    int updateLawServPic(String imgurl,int id);
}
