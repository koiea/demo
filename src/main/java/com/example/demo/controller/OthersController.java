package com.example.demo.controller;


import com.example.demo.service.OthersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class OthersController {
    @Autowired
    OthersService othersService;

    @RequestMapping("getAboutUs")
    public String getAboutUs(){

        return othersService.getAboutUs().toJSONString();
    }

    @RequestMapping("editAboutUs")
    public int editAboutUs(@RequestParam("title") String title, @RequestParam("shorttitle") String shorttitle, @RequestParam("text") String text,@RequestParam("file") MultipartFile file){
        othersService.editAboutUs(title,shorttitle,text,file);
        return 0;
    }

    @RequestMapping("getContact")
    public String getConatct(){
        return othersService.getContact().toJSONString();
    }

    @RequestMapping("editContact")
    public int editContact(@RequestParam("phone") String phone,@RequestParam("cellphone") String cellphone,@RequestParam("email") String email,@RequestParam("zip") String zip,@RequestParam("addr") String addr,@RequestParam("fax") String fax){
        return othersService.editContact(phone,cellphone,email,zip,addr,fax);
    }
}
