package com.example.demo.mapper;


import com.example.demo.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    List<User> selectAllUser();
    List<User> selectByNameLevelAddr(String name, String level, String addr);
    int insert(String name, String password, int sex, String cp, String addr, int state, int level);
    int updateUser(int id, String field, String value);
    int updateUser(int id, String field, int value);
    int delUser(int id);
    User selectSingleUserById(int id);
    User selectSingleUserByName(String name);
}
