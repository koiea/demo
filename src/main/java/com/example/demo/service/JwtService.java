package com.example.demo.service;



import com.example.demo.bean.LoginParams;
import com.example.demo.util.CommonResult;
import com.example.demo.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.HashMap;

@Service
public class JwtService {

    @Value("${jwt.tokenHead}")
    String tokenHead;

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    public CommonResult JwtLogin(LoginParams loginParams){
        HashMap<String, String> data = new HashMap<>();
        String token = null;
        try {
            token = generateTokenByMatchLoginParam(loginParams);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.validateFailed("用户名或密码错误");
        }
        if (StringUtils.isEmpty(token)){
            return CommonResult.validateFailed("用户名或密码错误");
        }
        data.put("tokenHead",tokenHead);
        data.put("access_token",token);
        return CommonResult.success(data);
    }

    public CommonResult JwtLogin(String userName,String password){
        HashMap<String, String> data = new HashMap<>();
        String token = null;
        try {
            token = generateTokenByMatchPw(userName,password);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.validateFailed("用户名或密码错误");
        }
        if (StringUtils.isEmpty(token)){
            return CommonResult.validateFailed("用户名或密码错误");
        }
        data.put("tokenHead",tokenHead);
        data.put("access_token",token);
        return CommonResult.success(data);
    }

    public String generateTokenByMatchLoginParam(LoginParams loginPrams){
        String username = loginPrams.getUsername();
        Assert.notNull(username,"账号必须不能为空");
        String password = loginPrams.getPassword();
        Assert.notNull(password,"密码必须不能为空");
        UserDetails user = userService.loadUserByUsername(username);
//        boolean matches = passwordEncoder.matches(password, user.getPassword());
        if(password.equals(user.getPassword())){
            return jwtTokenUtil.generateToken(user);
        }
        return null;
    }

    public String generateTokenByMatchPw(String userName,String password){

        UserDetails user = userService.loadUserByUsername(userName);
        boolean matches = passwordEncoder.matches(password, user.getPassword());
        if(matches){
            return jwtTokenUtil.generateToken(user);
        }
        return null;
    }

}
