package com.example.demo.mapper;



import com.example.demo.bean.News;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface NewsMapper  {
    List<News> selectNewsSample();
    List<News> selectMainpageNewsSample();
    News selectNewsContent(int id);
    int insertNews(String title,String date,String sample,String content,String type,int mainpage);
    int delNews(int id);
    int updateNews(int id,String title,String date,String sample,String content,int mainpage,String type);
}
