package com.example.demo.bean;

import io.swagger.annotations.ApiModel;

@ApiModel(value="律师对象", description="")
public class Lawer {
    int id;
    String name;
    String position;
    String introduction;
    String imgurl;
    String ctime;
    String etime;
    int enable;

    @Override
    public String toString() {
        return "Lawer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", introduction='" + introduction + '\'' +
                ", imgurl='" + imgurl + '\'' +
                ", ctime='" + ctime + '\'' +
                ", etime='" + etime + '\'' +
                ", enable=" + enable +
                ", mainpage=" + mainpage +
                '}';
    }

    int mainpage;

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getMainpage() {
        return mainpage;
    }

    public void setMainpage(int mainpage) {
        this.mainpage = mainpage;
    }

    public Lawer(int id, String name, String position, String introduction, String imgurl, String ctime, String etime, int enable, int mainpage) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.introduction = introduction;
        this.imgurl = imgurl;
        this.ctime = ctime;
        this.etime = etime;
        this.enable = enable;
        this.mainpage = mainpage;
    }

    public Lawer(int id, String name, String position, String introduction, String imgurl) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.introduction = introduction;
        this.imgurl = imgurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

}
