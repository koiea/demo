package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.bean.LawUserRole;
import com.example.demo.mapper.LawUserRoleMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
@Service
public class LawUserRoleService extends ServiceImpl<LawUserRoleMapper, LawUserRole>{

}
