package com.example.demo.util;

import com.alibaba.fastjson.JSONObject;

public class LayTableWrapper {
    public JSONObject layTableJSO(Long count){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",count);
        return  jsonObject;
    }
    public JSONObject layTableJSO(int count){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",count);
        return  jsonObject;
    }
}
