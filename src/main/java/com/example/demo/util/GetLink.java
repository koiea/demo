package com.example.demo.util;

public class GetLink {
    public static String getVirtualLink(String url){
        String[] r=url.split("\\\\");
        return "./images/"+r[r.length-1];
    }

    public static String getRealLink(String url){
        String[] r=url.split("\\\\");
        return "./src/main/resources/static/uploadimg"+r[r.length-1];
    }
}
