package com.example.demo.service;

import com.example.demo.bean.Admin;
import com.example.demo.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    @Autowired
    AdminMapper adminMapper;
    Admin admin;

    public int checkUser(String username){

        if(adminMapper ==null){
            System.out.println("usermapperisnull");
        }
        admin = adminMapper.getUser(username);
        if(admin ==null){
            return 0;
        }
        else{
            return 1;
        }
    }

    public Admin getAdmin() {
        return admin;
    }
}
