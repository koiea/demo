package com.example.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {






    @Bean
    public Queue emailTempQueue() {
        Map<String,Object> arguments = new HashMap<String,Object>();

//        arguments.put("x-message-ttl", 5000);
        arguments.put("x-dead-letter-exchange","emailDirectExchange");
        arguments.put("x-dead-letter-routing-key","email");
        return new Queue("emailTempQueue",true,false,false,arguments);
    }

    @Bean
    public Queue emailQueue() {
        return new Queue("emailQueue",true);
    }

    @Bean
    DirectExchange emailDirectExchange() {
        return new DirectExchange("emailDirectExchange",true,false);
    }

    @Bean
    Binding emailTemp() {
        return BindingBuilder.bind(emailTempQueue()).to(emailDirectExchange()).with("tempEmail");
    }

    @Bean
    Binding email() {
        return BindingBuilder.bind(emailQueue()).to(emailDirectExchange()).with("email");
    }



}