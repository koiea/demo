package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.bean.LawUserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
public interface LawUserRoleMapper extends BaseMapper<LawUserRole> {

}
