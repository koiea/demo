package com.example.demo.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.mapper.OthersMapper;
import com.example.demo.util.GetLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Service
public class OthersService {
    @Autowired
    OthersMapper othersMapper;

    @Autowired
    StorageService storageService;

    public JSONArray getAboutUs(){
        HashMap hashMap=othersMapper.selectAboutUs();
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("title",hashMap.get("title").toString());
        jsonObject.put("shorttitle",hashMap.get("shorttitle").toString());
        jsonObject.put("text",hashMap.get("text").toString());
        jsonObject.put("url", GetLink.getVirtualLink(hashMap.get("url").toString()));
        jsonArray.add(jsonObject);
        return  jsonArray;
    }

    public int editAboutUs(String title, String shorttitle, String text, MultipartFile file){
        String url=othersMapper.selectAboutUs().get("url").toString();
        othersMapper.updateAboutUs(title,shorttitle,text,storageService.store(file));
        storageService.deleteOne(url);
        return 0;
    }

    public JSONArray getContact(){
        HashMap hashMap=othersMapper.selectContact();
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("phone",hashMap.get("phone").toString());
        jsonObject.put("fax",hashMap.get("fax").toString());
        jsonObject.put("cellphone",hashMap.get("cellphone").toString());
        jsonObject.put("zip",hashMap.get("zip").toString());
        jsonObject.put("email",hashMap.get("email").toString());
        jsonObject.put("addr",hashMap.get("addr").toString());
        jsonArray.add(jsonObject);
        return jsonArray;
    }

    public int editContact(String phone,String cellphone,String email,String zip,String addr,String fax){
        return othersMapper.updateContact(phone,cellphone,email,zip,addr,fax);
    }
}
