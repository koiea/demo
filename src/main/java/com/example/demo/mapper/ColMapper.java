package com.example.demo.mapper;


import  com.example.demo.bean.Col;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColMapper {
    List<Col> getColList();
    int delCol(int id);
    int insertCol();
    int updateCol(int id,String col);
}
