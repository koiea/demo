package com.example.demo.bean;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;

@ApiModel(value="法律服务对象", description="")
public class LawServ {

    int id;

    String text;

    String url;

    String price;

    String content;

    int idlaw_servtype;


    int enable;
    String ctime;
    String etime;
    String type;


    public LawServ(int id, String text, String url, String price, String content, int idlaw_servtype, int enable, String ctime, String etime) {
        this.id = id;
        this.text = text;
        this.url = url;
        this.price = price;
        this.content = content;
        this.idlaw_servtype = idlaw_servtype;
        this.enable = enable;
        this.ctime = ctime;
        this.etime = etime;
    }

    public LawServ(int id, String text, String url, String price, String content, int idlaw_servtype, int enable, String ctime, String etime, String type) {
        this.id = id;
        this.text = text;
        this.url = url;
        this.price = price;
        this.content = content;
        this.idlaw_servtype = idlaw_servtype;
        this.enable = enable;
        this.ctime = ctime;
        this.etime = etime;
        this.type = type;
    }



    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public LawServ(int id, String text, String url, String price, String content, int idlaw_servtype, String type) {
        this.id = id;
        this.text = text;
        this.url = url;
        this.price = price;
        this.content = content;
        this.idlaw_servtype=idlaw_servtype;
        this.type = type;
    }
    public LawServ(int id, String text, String url, String price, String content, int idlaw_servtype) {
        this.id = id;
        this.text = text;
        this.url = url;
        this.price = price;
        this.content = content;
        this.idlaw_servtype = idlaw_servtype ;
    }

    public LawServ(int id, String text, String url) {
        this.id = id;
        this.text = text;
        this.url = url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdlaw_servtype() {
        return idlaw_servtype;
    }

    public void setIdlaw_servtype(int idlaw_servtype) {
        this.idlaw_servtype = idlaw_servtype;
    }

}
