package com.example.demo.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.bean.User;
import com.example.demo.mapper.LawUserRoleMapper;
import com.example.demo.mapper.PermissionManagerMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.util.LayTableWrapper;
import com.example.demo.util.MD5Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PermissionManagerMapper permissionManagerMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public JSONObject getAllUserTable(){
        LayTableWrapper layTableWrapper=new LayTableWrapper();
        List<User> userList=userMapper.selectAllUser();
        JSONObject jsonObject=layTableWrapper.layTableJSO(userList.size());
        jsonObject.put("data",JSON.toJSON(userList));
        return  jsonObject;
    }

    public JSONObject getUserByNameAddr(String name,String addr){
        LayTableWrapper layTableWrapper=new LayTableWrapper();
        List<User> userList=userMapper.selectByNameAddr(name,addr);
        JSONObject jsonObject=layTableWrapper.layTableJSO(userList.size());
        jsonObject.put("data",JSON.toJSON(userList));
        return  jsonObject;
    }

    public int addAccount(String name,String password,int sex,String cp,String addr,int state,String email){
        try{
            return userMapper.insert(name, passwordEncoder.encode(password),sex,cp,addr,state,email);
        }catch(Exception e){
            System.out.println(e);
            return 0;
        }
    }

//    @CacheEvict(value = "User",allEntries=true)
    public int editAccount(int id,String field,String value){
        return userMapper.updateUser(id,field,value);
    }

    @CacheEvict(value = "User",allEntries=true)
    public int editAccount(int id,String field,int value){
        return userMapper.updateUser(id,field,value);
    }

//    @CacheEvict(value = "User",allEntries=true)
    public int delAccount(int id){
        return userMapper.delUser(id);
    }


    public int verifyPW(int id,String pw){
        try{
            User user=userMapper.selectSingleUserById(id);
            if(user.getPassword().equals(passwordEncoder.encode(pw))) {
                return 1;
            }else{
                return 0;
            }
        }catch (Exception e){
            System.out.println(e);
            return 0;
        }

    }

//    @CacheEvict(value = "User",allEntries=true)
    public int editPWAccount(int id, String opw,String npw){
        if(verifyPW(id,opw)==1){
            try{
                return editAccount(id,"password", passwordEncoder.encode(npw));
            }catch (Exception e){
                System.out.println(e);
            }
        }
        return 0;
    }



    public User getUserByName(String name){
        return  userMapper.selectSingleUserByName(name);
    }

    @Override
//    @Cacheable(value = "User" , key="'name:'+#s")
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user=userMapper.selectSingleUserByName(s);
        int id=user.getIdlawUser();
        user.setAuthorities(permissionManagerMapper.selectPermissionByIdlaw_user(id));
        return user;
    }
}
