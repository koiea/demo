package com.example.demo.mapper;


import com.example.demo.bean.Lawer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface LawerMapper {
    public int insertLawer(String name,String position,String introduction,String imgurl);
    public List<Lawer> selectAllLawer();
    public List<Lawer> selectMainpageLawer();
    public List<Lawer> selectEnableLawer();
    public int delLawer(int id);
    public Lawer selectSingleLawer(int id);
    int updateSingleLawer(int id,String field,String value);
}
