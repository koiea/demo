package com.example.demo;

import com.example.demo.bean.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.service.NewsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private NewsMapper newsMapper;
	@Test
	void contextLoads() {
		List<News> newsList = newsMapper.selectNewsSample();
		for (News news : newsList) {
			System.out.println(news);
		}
	}

}
