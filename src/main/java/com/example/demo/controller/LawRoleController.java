package com.example.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.demo.bean.LawRole;
import com.example.demo.bean.LawUserRole;
import com.example.demo.service.LawRoleService;
import com.example.demo.service.LawUserRoleService;
import com.example.demo.util.CommonResult;
import com.example.demo.util.LayTableWrapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.Launcher;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */

@Api(tags = "角色控制类")
@RestController
@RequestMapping("/role")
public class LawRoleController {

    @Autowired
    LawRoleService lawRoleService;

    @Autowired
    LawUserRoleService lawUserRoleService;


    @RequestMapping("/getTable")
    public JSONObject getRoleTable(@RequestParam("page") Long page,@RequestParam("limit") Long limit){
        IPage iPage=lawRoleService.getRoleTable(page,limit);
        JSONObject jsonObject=new LayTableWrapper().layTableJSO(iPage.getTotal());
        jsonObject.put("data",iPage.getRecords());
        return jsonObject;
    }

    @PostMapping("/editRolePermission")
    public int editRolePermission(@RequestParam("id") int id,@RequestParam("checkedData") String checkData){
        return lawRoleService.editRolePermission(id,checkData);


//        JSONArray jsonArray=JSONArray.parseArray(checkData);
//        System.out.println(jsonArray);
//        System.out.println(jsonArray.get(0));
//        for(int i=0;i<jsonArray.size();i++){
//            JSONObject jsonObject=jsonArray.getJSONObject(i);
//            System.out.println(jsonObject.getJSONArray("children"));
//        }
//        我错了
    }

    @PostMapping("/getRolePermissionByRoleId")
    public CommonResult getRolePermissionByRoleId(@RequestParam("id") Long id){
        return lawRoleService.getRolePermissionByRoleId(id);
    }

    @PostMapping("/addRole")
    public CommonResult addRole(@RequestParam("role") String role){
        return lawRoleService.addRole(role);
    }

    @PostMapping("/deleteRole")
    public CommonResult delete(@RequestParam("id") Long id){
        return lawRoleService.deleteRole(id);
    }

    @PostMapping("/getRoleTableWithUser")
    public JSONObject getRoleTableWithUser(@RequestParam("idUser") Long idUser, @RequestParam("page") Long page,@RequestParam("limit") Long limit){
        Map map=lawRoleService.getRoleTableWithUserId(idUser,page,limit);
        JSONObject jsonObject=new LayTableWrapper().layTableJSO((Long)map.get("size"));
        jsonObject.put("data", map.get("data"));
        return jsonObject;
    }

    @CacheEvict(value = "User",allEntries=true)
    @PostMapping("/editUserRole")
    public int editUserRole(@RequestParam("id") int id,@RequestParam("idUser") int idUser,@RequestParam("value") Long value){
        if(value==1){
            return lawUserRoleService.save(new LawUserRole().setIdlawRole(id).setIdlawUser(idUser))?1:0;
        }else{
            return lawUserRoleService.remove(new QueryWrapper<LawUserRole>().eq("idlaw_user",idUser).eq("idlaw_role",id))?1:0;
        }
    }

}

