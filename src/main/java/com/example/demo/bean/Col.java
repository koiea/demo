package com.example.demo.bean;

public class Col {
    int id;
    String col;

    public Col(int id, String col) {
        this.id = id;
        this.col = col;
    }

    public Col(String col) {
        this.id=0;
        this.col = col;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    @Override
    public String toString() {
        return "Col{" +
                "id=" + id +
                ", col='" + col + '\'' +
                '}';
    }
}
