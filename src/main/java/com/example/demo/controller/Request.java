package com.example.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.service.ColService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@RestController
public class Request {

    @Autowired
    ColService colService;

    @RequestMapping("test")
    public String rdata(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",1);
        jsonObject.put("data",colService.getColJSA());
        return jsonObject.toJSONString();
    }

//    @PostMapping("del")
//    public void del(@RequestParam("id") int id){
//        colService.delCol(id);
//    }
//
//    @PostMapping("add")
//    public  void add(){
//        colService.insertCol();
//    }
//
//    @PostMapping("edit")
//    public void update(@RequestParam("id") int id,@RequestParam("col") String col){
//        colService.updateCol(id,col);
//    }


}
