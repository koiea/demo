package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.bean.LawRolePermission;
import com.example.demo.mapper.LawRolePermissionMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
@Service
public class LawRolePermissionService extends ServiceImpl<LawRolePermissionMapper, LawRolePermission>{

}
