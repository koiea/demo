package com.example.demo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LawUserRole对象", description="")
public class LawUserRole implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "idlaw_user_role", type = IdType.AUTO)
    private Integer idlawUserRole;

    private Integer idlawUser;

    private Integer idlawRole;


}
