package com.example.demo.bean;


import lombok.Data;
import org.springframework.context.annotation.Bean;

@Data
public class LawRoleWithUser extends LawRole {
    int toUser;
}
