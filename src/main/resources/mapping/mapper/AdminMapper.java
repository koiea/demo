package com.example.demo.mapper;

import com.example.demo.bean.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface AdminMapper {
    Admin getUser(String username);
}
