package com.example.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;


@Repository
@Mapper
public interface OthersMapper {
    public HashMap selectAboutUs();
    public int updateAboutUs(String title,String shorttitle,String text,String url);
    public HashMap selectContact();
    public int updateContact(String phone,String cellphone,String email,String zip,String addr,String fax);
}
