package com.example.demo.controller;


import com.example.demo.service.AdminService;
import com.example.demo.service.JwtService;
import com.example.demo.service.UserService;
import com.example.demo.util.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "登录控制类")
@RestController
public class LoginController {

    @Autowired
    JwtService jwtService;

    @Autowired
    UserService userService;

//    @RequestMapping("jumpLogin")
//    public String jumpLogin(){
//        return "login";
//    }

    @RequestMapping("login")
    public CommonResult login(HttpServletRequest request, @RequestParam("username") String username, @RequestParam("password") String password) {
        return jwtService.JwtLogin(username,password);
    }


//    public int login(HttpServletRequest request, @RequestParam("username") String username, @RequestParam("password") String password){
//        if(userService.verifyNameAccount(username,password)==1){
//            request.getSession().setAttribute("userid", userService.getUserByName(username).getIdlaw_user());
//                request.getSession().setMaxInactiveInterval(300);
//                try{
//                    System.out.println(username+password);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//        }
//        return 0;
//    }

//        if(adminService.checkUser(username)==1){
//            if(adminService.getAdmin().getPassword().equals(password)){
//
//                request.getSession().setAttribute("userid", adminService.getAdmin().getIduser());
//                request.getSession().setMaxInactiveInterval(300);
//                try{
//                    System.out.println(username+password);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//                return adminService.getAdmin().getIduser();
//            }
//            return 0;
//        }
//        return 0;
//    }


    @RequestMapping("/exit")
    public int exit(HttpServletRequest request){
        request.getSession().invalidate();
        return 0;
    }
}
