package com.example.demo.listener;

import com.example.demo.bean.User;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class EmailQueueReceiver {

    @RabbitListener(queues = {"emailQueue"})
    public void sendEmail(User user) {

        try {
            System.out.println(user);

        }catch (Exception e){
            e.printStackTrace();
        }

        return;
    }
}
