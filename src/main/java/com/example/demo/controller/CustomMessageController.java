package com.example.demo.controller;


import com.alibaba.fastjson.JSONObject;
import com.example.demo.service.CustomMessageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "用户留言控制类")
public class CustomMessageController {


    @Autowired
    CustomMessageService customMessageService;

//    @RequestParam("name") String name,@RequestParam("phone") String phone,@RequestParam("email") String email,@RequestParam("message") String message


    @PostMapping("saveMessage")
    public int saveMessage(@RequestParam("name") String name,@RequestParam("phone") String phone,@RequestParam("email") String email,@RequestParam("message") String message){
        if("".equals(name)){
            name="notext";
        }
        if("".equals(phone)){
            phone="notext";
        }
        if(email.equals("")){
            email="notext";
        }
        if(message.equals("")){
            message="notext";
        }

        customMessageService.insertToDB(name,phone,email,message);
        return 0;
    }

    @PostMapping("getMessage")
    public String getMessage(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",1);
        jsonObject.put("data",customMessageService.getAllCustomMessage());
        return jsonObject.toJSONString();
    }

    @PostMapping("deleteMessage")
    public int deleteMessage(@RequestParam("id") int id){
        return customMessageService.deleteCustomMessage(id);
    }
}
