package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class PageControl {
    @RequestMapping("order")
    public  String testJumpPage(){
        System.out.println("jump");
        return "redirect:order-list1.html";
    }
}
